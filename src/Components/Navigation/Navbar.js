import React, { Component } from "react";
import "./Navbar.css";
import { FaMapMarkerAlt } from "react-icons/fa";
import Login from "../Login/Login";
import Signup from "../Signup/Signup";
export class Navbar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoggedIn: false,
    };
  }

  render() {
    return (
      <div className="d-flex justify-content-around">
        <nav className="navbar navbar-light container-lg">
          <a className="navbar-brand mr-5" href="/">
            ZOMATO
          </a>
          <div
            className="border-3 shadow-sm rounded d-flex "
            id="search-fields"
          >
            <span className="location-marker">
              <FaMapMarkerAlt />
            </span>
            <input
              className="form-control mr-sm-2 border-right"
              type="search"
              placeholder="Search city"
              aria-label="Search"
            />{" "}
            <input
              className="form-control mr-sm-2"
              type="search"
              placeholder="Search by cuisine"
              aria-label="Search"
            />
          </div>
          {/* {this.state.isLoggedIn ? (
            <>Hello User</>
          ) : (
            <div className="d-flex user-buttons">
              <Signup />
              <Login checkIfLoggedIn={this.checkIfLoggedIn} />
            </div>
          )} */}
          <div className="d-flex user-buttons">
            <Signup user={this.props.user} />
            <Login user={this.props.user} />
          </div>
          {/* {
            this.props.user
              ? <>{this.props.user.user_name}</>
              : <div className="d-flex user-buttons">
                <Signup/>
                <Login/>
              </div>
          } */}
        </nav>
      </div>
    );
  }
}

export default Navbar;

// {this.state.isLoggedIn
//   ? <>Hello</>
//   : <>Bye</>
//   }
