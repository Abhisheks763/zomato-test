import React, { Component } from "react";
import "./Deliveryoptions.css";
export class Deliveryoptions extends Component {
  render() {
    return (
      <div className="d-flex justify-content-center">
        <div className="container-fluid border-bottom">
          <div className="container-lg">
            <div className="row mt-2 pb-1 ">
              <div className="col-3 d-flex pb-2">
                <div className="rounded-circle bg-light mr-2">
                  <img
                    className="deliveryoptions-img"
                    alt="illustration"
                    src="https://b.zmtcdn.com/data/o2_assets/c0bb85d3a6347b2ec070a8db694588261616149578.png"
                  />
                </div>
                <div className="d-flex justify-content-center align-items-center">
                  Delivery
                </div>
              </div>
              <div className="col-3 d-flex pb-2">
                {" "}
                <div className="rounded-circle bg-light mr-2">
                  <img
                    className="deliveryoptions-img"
                    alt="illustration"
                    src="https://b.zmtcdn.com/data/o2_assets/30fa0a844f3ba82073e5f78c65c18b371616149662.png"
                  />
                </div>
                <div className="d-flex justify-content-center align-items-center">
                  Dining out
                </div>
              </div>
              <div className="col-3 d-flex pb-2">
                {" "}
                <div className="rounded-circle bg-light mr-2">
                  <img
                    className="deliveryoptions-img"
                    alt="illustration"
                    src="https://b.zmtcdn.com/data/o2_assets/855687dc64a5e06d737dae45b7f6a13b1616149818.png"
                  />
                </div>
                <div className="d-flex justify-content-center align-items-center">
                  Nightlife
                </div>
              </div>
              <div className="col-3 d-flex pb-2">
                {" "}
                <div className="rounded-circle bg-light mr-2">
                  <img
                    className="deliveryoptions-img"
                    alt="illustration"
                    src="https://b.zmtcdn.com/data/o2_assets/0f6dcb1aef93fa03ea3f91f37918f3bc1616649503.png"
                  />
                </div>
                <div className="d-flex justify-content-center align-items-center">
                  Nutrition
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Deliveryoptions;
