import React, { Component } from "react";
import { Card } from "react-bootstrap";
import "./Collection.css";

export default class Collection extends Component {
  render() {
    return (
      <div>
        <Card className="bg-dark text-white collection">
          <Card.Img
            src={
              "https://b.zmtcdn.com/web_assets/81f3ff974d82520780078ba1cfbd453a1583259680.png"
            }
            alt="Card image"
          />
          <Card.ImgOverlay>
            <div className="card-text">{this.props.text}</div>
          </Card.ImgOverlay>
        </Card>
      </div>
    );
  }
}
