import React, { Component } from "react";
import "./PromotedResCard.css";
export class PromotedResCard extends Component {
  render() {
    return (
      <div>
        <div className="card m-1 position-relative" style={{ width: "18rem" }}>
          <img
            src="https://b.zmtcdn.com/data/pictures/6/57866/1b294212d1c5d909d00e583a1ada0a3e_o2_featured_v2.jpg?output-format=webp"
            className="card-img-top rounded"
            alt="..."
          />
          <span className=" bg-light px-1" id="discount">
            40% Off
          </span>
          <div className="card-body p-2 ">
            <h4 className="mb-1">{this.props.name}</h4>
            <p className="card-text mb-1">
              South Indian, Chinese, North Indian
            </p>
            <p className="mb-1">
              {" "}
              ₹100 for one <span> 47 min</span>
            </p>
            <p className="mb-1">{this.props.address}</p>
            <p className="mb-1">{this.props.contact}</p>
            <p className="mb-1 d-inline mr-3 bg-success text-white border-">
              Promoted
            </p>
            <a
              className="d-inline-block bg-danger rounded text-white px-1"
              href={`/restaurant/${this.props.id}/order`}
            >
              Order
            </a>
          </div>
        </div>
      </div>
    );
  }
}

export default PromotedResCard;
