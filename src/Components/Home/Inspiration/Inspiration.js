import "./Inspiration.css";

import React, { Component } from "react";
import Row from "./Row";

export default class Inspiration extends Component {
  render() {
    return (
      <div>
        <div className="inspiration-container" id="inspiration-container">
          <h3 className="text-center ">Inspiration for your first order </h3>

          <div
            id="carouselExampleIndicators"
            className="carousel slide"
            data-ride="carousel"
          >
            <div className="carousel-inner container">
              <div className="carousel-item active">
                <Row />
              </div>

              <div className="carousel-item">
                <Row />
              </div>

              <div className="carousel-item">
                <Row />
              </div>
            </div>

            <a
              className="carousel-control-prev"
              href="#carouselExampleIndicators"
              role="button"
              data-slide="prev"
            >
              <span
                className="carousel-control-prev-icon"
                aria-hidden="true"
              ></span>
              <span className="sr-only">Previous</span>
            </a>
            <a
              className="carousel-control-next"
              href="#carouselExampleIndicators"
              role="button"
              data-slide="next"
            >
              <span
                className="carousel-control-next-icon"
                aria-hidden="true"
              ></span>
              <span className="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
    );
  }
}
