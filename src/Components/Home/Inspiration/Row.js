import React, { Component } from 'react';
import RowItem from './RowItem';

export default class Row extends Component {
    render() {
        return (
            <div className="row">
                <RowItem order="momos"/>
                <RowItem order="rolls"/>
                <RowItem order="burgers"/>
            </div>
        )
    }
}
