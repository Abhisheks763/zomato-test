import React, { Component } from 'react';

export default class RowItem extends Component {
    render() {
        return (
            <div className="col-4 one-item ">
                <div className="card" style={{ width: "18rem" }}>
                    <img src={"https://b.zmtcdn.com/web_assets/81f3ff974d82520780078ba1cfbd453a1583259680.png"} className="card-img-top" alt="..." />
                    <div className="card-body">
                        <p className="card-text">{this.props.order}</p>
                    </div>
                </div>
            </div>

        )
    }
}
