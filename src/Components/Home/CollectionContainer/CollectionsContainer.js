import React, { Component } from "react";
import "./CollectoinsContainer.css";
import Collection from "../Collections/Collection";

export default class CollectionsContainer extends Component {
  render() {
    return (
      <div className="mt-2">
        <h3>Collections</h3>
        <h4>
          Explore curated lists of top restaurants, cafes, pubs, and bars in
          Delhi NCR, based on trends
        </h4>
        <div className="collection-container">
          <Collection text="Eid Mubarak!" />
          <Collection text="Newly Opened" />
          <Collection text="Trending This Week" />
          <Collection text="Best of Delhi NCR" />
        </div>
      </div>
    );
  }
}
