import React, { Component } from "react";
import "./City.css";

export default class City extends Component {
  render() {
    return (
      <div className="city">
        <a href="/">{this.props.cityName}</a>
      </div>
    );
  }
}
