import React, { Component } from "react";
import "./RestaurantType.css";

export default class RestaurantType extends Component {
  render() {
    return (
      <>
        <a className="restaurant-type" href="/">
          Restaurant near me
        </a>
        <span>.</span>
      </>
    );
  }
}
