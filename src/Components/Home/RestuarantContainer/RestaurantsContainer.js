import React, { Component } from "react";
import Restaraunt from "../Restaurants/Restaraunt";
import "./RestaurantsContainer.css";

export default class RestaurantsContainer extends Component {
  render() {
    return (
      <div style={{ margin: "50px 0px" }}>
        <h4>Top Restaurant Chains </h4>
        <div className="restaurant-container">
          <Restaraunt />
          <Restaraunt />
          <Restaraunt />
          <Restaraunt />
          <Restaraunt />
          <Restaraunt />
          <Restaraunt />
          <Restaraunt />
          <Restaraunt />
          <Restaraunt />
          <Restaraunt />
          <Restaraunt />
          <Restaraunt />
          <Restaraunt />
          <Restaraunt />
          <Restaraunt />
          <Restaraunt />
          <Restaraunt />
          <Restaraunt />
          <Restaraunt />
          <Restaraunt />
          <Restaraunt />
        </div>
      </div>
    );
  }
}
