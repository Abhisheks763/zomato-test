import React, { Component } from "react";
import "./Signup.css";
import Joi from "joi";
import Error from "../Error/Error";

import API from "../../BaseURL";

export class Signup extends Component {
  constructor() {
    super();
    this.state = {
      Fullname: "",
      email: "",
      termNCondn: null,
      error: "",
      errorIdentifier: "",
      password: "",
      repeat_password: "",
      validationError: false,
    };
  }
  onChange = (e) => {
    // console.log(e.target.value);
    this.setState({ [e.target.name]: e.target.value });
    // console.log(this.state);
  };
  termNCondnChange = (e) => {
    this.setState({ termNCondn: e.target.checked }, () => {
      // console.log(this.state);
    });
  };
  signUpCheck = (e) => {
    const { Fullname, email, termNCondn, password, repeat_password } =
      this.state;

    //Joi validation
    const schema = Joi.object({
      // Fullname: Joi.string().required(),
      Fullname: Joi.string().alphanum().min(5).max(40).required(),
      email: Joi.string().email({
        minDomainSegments: 2,
        tlds: { allow: ["com", "net", "tech", "io"] },
      }),
      password: Joi.string().pattern(
        new RegExp(
          /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,20}$/
        )
      ),
      repeat_password: Joi.ref("password"),

      termNCondn: Joi.boolean().invalid(false),
    });
    const result = schema.validate({
      Fullname,
      email,
      termNCondn,
      password,
      repeat_password,
    });
    console.log(result);

    //Set state according to obtained error
    if (result.error) {
      e.preventDefault();
      //error msg when error is in password field
      if (result.error.details[0].context.key === "password") {
        this.setState({
          error:
            "The password should contain atleast 1 lower case character, 1 upper case character, 1 special character and should have a length between 8 to 20",
        });
        this.setState({ errorIdentifier: result.error.details[0].context.key });
      } else {
        //error msg when error is in repeat password field
        if (result.error.details[0].context.key === "repeat_password") {
          this.setState({ error: "Password didn't match" });
          this.setState({
            errorIdentifier: result.error.details[0].context.key,
          });
        } else {
          //error msg when error is else where
          this.setState({
            errorIdentifier: result.error.details[0].context.key,
          });
          this.setState(
            { error: result.error.details[0].message.replaceAll('"', "") },
            () => {
              // console.log(this.state);
            }
          );
        }
      }
    } else {
      //when there is no error
      this.setState({ error: "" }, () => {});
      this.setState({ errorIdentifier: "" });
      // console.log(this.state);
      e.preventDefault();
      //SENDING POST REQ FOR SIGNUP

      const data = {
        user_name: this.state.Fullname,
        email_address: this.state.email,
        password: this.state.password,
      };
      console.log(data);
      // axios
      //   .post("https://zomato-clone-server.herokuapp.com/register", data)
      API.post(`/register`, data)
        .then((res) => {
          this.setState({ validationError: false });
          console.log(res);
        })
        .catch((error) => {
          this.setState({ validationError: true });
          console.log(error);
        });
    }
  };
  render() {
    return (
      <div className="container">
        <button
          type="button"
          className="btn btn-default"
          data-toggle="modal"
          data-target="#staticBackdrop"
        >
          {this.props.user ? <></> : <>Signup</>}
        </button>

        <div
          className="modal fade"
          id="staticBackdrop"
          data-backdrop="static"
          data-keyboard="false"
          tabIndex="-1"
          aria-labelledby="staticBackdropLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="staticBackdropLabel">
                  Zomato
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <form className="needs-validation" noValidate>
                  <div className="form-row">
                    <div className="col-md-12 mb-3">
                      <input
                        type="text"
                        className="form-control full-width border"
                        required
                        placeholder="Username"
                        name="Fullname"
                        value={this.state.Fullname}
                        onChange={this.onChange}
                      />
                    </div>
                  </div>
                  {this.state.errorIdentifier === "Fullname" ? (
                    <Error error={this.state.error} />
                  ) : (
                    <></>
                  )}
                  <div className="form-row">
                    <div className="col-md-12 mb-3">
                      <input
                        type="email"
                        className="form-control full-width border"
                        required
                        placeholder="Email"
                        name="email"
                        value={this.state.email}
                        onChange={this.onChange}
                      />
                    </div>
                  </div>

                  {this.state.errorIdentifier === "email" ? (
                    <Error error={this.state.error} />
                  ) : (
                    <></>
                  )}
                  <div className="form-row">
                    <div className="col-md-12 mb-3">
                      <input
                        type="password"
                        className="form-control full-width  border"
                        required
                        placeholder="Enter password"
                        name="password"
                        value={this.state.password}
                        onChange={this.onChange}
                      />
                    </div>
                  </div>
                  {this.state.errorIdentifier === "password" ? (
                    <Error error={this.state.error} />
                  ) : (
                    <></>
                  )}
                  <div className="form-row">
                    <div className="col-md-12 mb-3">
                      <input
                        type="password"
                        className="form-control full-width border"
                        required
                        placeholder="Confirm password"
                        name="repeat_password"
                        value={this.state.repeat_password}
                        onChange={this.onChange}
                      />
                    </div>
                  </div>
                  {this.state.errorIdentifier === "repeat_password" ? (
                    <Error error={this.state.error} />
                  ) : (
                    <></>
                  )}
                  <div className="form-group text-center align-middle justify-content-center mb-1">
                    <div className="form-check">
                      <input
                        className="form-check-input "
                        type="checkbox"
                        value=""
                        id="invalidCheck"
                        required
                        onChange={this.termNCondnChange}
                      />
                      <label
                        className="form-check-label "
                        htmlFor="invalidCheck"
                        id="tNc"
                      >
                        I agree to Zomato's <a href="/">Terms of Service</a>,
                        <a href="/">Privacy Policy</a>and
                        <a href="/">Content Policies</a>
                      </label>
                    </div>
                  </div>
                  {this.state.errorIdentifier === "termNCondn" ? (
                    <Error error={`Please agree to terms n conditions`} />
                  ) : (
                    <></>
                  )}
                  {this.state.validationError ? (
                    <Error error="User with this email address already exists" />
                  ) : (
                    <></>
                  )}
                  <button
                    className="btn btn-primary btn-block"
                    type="submit"
                    onClick={this.signUpCheck}
                  >
                    Create Account
                  </button>
                </form>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Signup;
