import React, { Component } from "react";
import "./Checkout.css";
import CheckoutItems from "../CheckoutItems";

export class Checkout extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const data = this.props.history.location.state.previousState;
    const array = [];
    for (let i in data) {
      if (i == "foodItems" || i == "totalPrice") {
      } else {
        array.push([i, data[i]]);
      }
    }

    const orderItems = array.map((arr) => {
      return <CheckoutItems itemname={arr[0]} quantity={arr[1]} />;
    });

    return (
      <div id="checkout-container" className="mx-4 border rounded p-2">
        <div className="container-lg">
          <h1>Checkout</h1>
        </div>

        <div className="container-lg">
          <div className="row justify-content-between">
            {" "}
            <div className="col-7">
              <div className="border rounded row justify-content-between mb-2 p-2">
                <h3>{this.props.user.user_name}</h3>
                <span>
                  <h5 className="text-muted">
                    {this.props.user.email_address}
                  </h5>
                </span>
              </div>
              <div className="row flex-column border rounded p-2 mb-2">
                <h4>Address</h4>
                <p>Bengaluru, Karnataka 560034</p>
              </div>
            </div>
            <div className="col-4 border rounded">
              <div className="row  ml-0">
                <h4>order summary</h4>
              </div>
              {orderItems}
              <div className="row align-items-center d-flex p-2 justify-content-between">
                <p className="m-0">Total</p>
                <p>₹ {data.totalPrice}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Checkout;
