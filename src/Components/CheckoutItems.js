import React, { Component } from 'react'

export default class CheckoutItems extends Component {
    render() {
        return (
            <>
                <div className="row align-items-center d-flex p-2 justify-content-between">
                    <p className="m-0">{this.props.itemname}</p>
                    <div
                        class="btn-group m-0"
                        role="group"
                        aria-label="Basic example"
                    >
                       {this.props.quantity}
                    </div>
                </div>
            </>
        )
    }
}
