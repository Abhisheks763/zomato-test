import React, { Component } from "react";
import "./Login.css";

import Joi from "joi";
import Error from "../Error/Error";
import API from "../../BaseURL";

export class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      error: "",
      errorIdentifier: "",
      validationError: false,
      redirect: false,
      anyErrors: true,
    };
  }

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  loginCheck = (e) => {
    const { email, password } = this.state;

    const schema = Joi.object({
      email: Joi.string().email({
        minDomainSegments: 2,
        tlds: { allow: ["com", "net", "tech", "io"] },
      }),
      password: Joi.string().pattern(
        new RegExp(
          /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,20}$/
        )
      ),
    });
    const result = schema.validate({ email, password });
    // console.log(result);

    if (result.error) {
      e.preventDefault();
      const errorMessage = result.error.details[0].message.replaceAll('"', "");
      //error msg when error is in password field
      if (result.error.details[0].context.key === "password") {
        this.setState({
          error:
            "The password should contain atleast 1 lower case character, 1 upper case character, 1 special character and should have a length between 8 to 20.",
        });
        this.setState({ errorIdentifier: result.error.details[0].context.key });
      } else {
        //error msg when error is not in passwrod
        this.setState({ error: errorMessage }, () => {});
        this.setState({ errorIdentifier: result.error.details[0].context.key });
      }
      // console.log(this.state);
    } else {
      e.preventDefault();
      this.setState(
        {
          error: "",
          errorIdentifier: "",
        },
        () => {}
      );
      // console.log(this.state);

      //SENDING POST REQ FOR LOGIN

      const data = {
        email_address: this.state.email,
        password: this.state.password,
      };

      // axios
      //   .post("https://zomato-clone-server.herokuapp.com/login", data)
      API.post(`/login`, data)
        .then((res) => {
          this.setState({
            validationError: false,
            redirect: true,
            anyErrors: false,
          });
          localStorage.setItem("token", res.data.token);
          console.log(res);
        })
        .catch((error) => {
          this.setState({ validationError: true });
          console.log(error);
        });
    }
  };

  isLogged = () => {
    if (!this.state.anyErrors) {
      return "modal";
    }
  };

  render() {
    return (
      <>
        <div className="container">
          <button
            type="button"
            className="btn btn-default"
            data-toggle="modal"
            data-target="#staticBackdrop2"
          >
            {this.props.user ? <>{this.props.user.user_name}</> : <>Login</>}
          </button>

          <div
            className="modal fade"
            id="staticBackdrop2"
            data-backdrop="static"
            data-keyboard="false"
            tabIndex="-1"
            aria-labelledby="staticBackdropLabel"
            aria-hidden="true"
          >
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title" id="staticBackdropLabel">
                    Login
                  </h5>
                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    aria-label="Close"
                  >
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div className="modal-body">
                  <form className="needs-validation" noValidate>
                    <div className="form-row">
                      <div className="col-md-12 mb-3">
                        <input
                          type="email"
                          className="form-control full-width border"
                          required
                          placeholder="Email"
                          name="email"
                          onChange={this.onChange}
                        />
                      </div>
                    </div>
                    <div className="form-row">
                      <div className="col-md-12 mb-3">
                        <input
                          type="password"
                          className="form-control full-width border"
                          required
                          placeholder="Password"
                          name="password"
                          onChange={this.onChange}
                        />
                      </div>
                    </div>
                    <Error error={this.state.error} />
                    <button
                      className="btn btn-primary btn-block"
                      type="submit"
                      onClick={this.loginCheck}
                      data-dismiss={this.isLogged()}
                    >
                      Login
                    </button>
                  </form>
                </div>
                {this.state.validationError ? (
                  <Error error="Email Address or password is Invalid !" />
                ) : (
                  <></>
                )}
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    data-dismiss="modal"
                  >
                    Close
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Login;
