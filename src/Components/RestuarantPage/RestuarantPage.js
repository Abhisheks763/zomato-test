import React, { Component } from "react";
import PhotoSlideBar from "./PhotoSlideBar/PhotoSlideBar";
import Menu from "./Menu/Menu";
export class RestuarantPage extends Component {
  render() {
    return (
      <div>
        <PhotoSlideBar restaurantID={this.props.match.params.restaurantID} />
        <Menu
          restaurantID={this.props.match.params.restaurantID}
          user={this.props.user}
        />
      </div>
    );
  }
}

export default RestuarantPage;
