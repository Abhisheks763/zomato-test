import React, { Component } from "react";
import MenuItem from "../MenuItem/MenuItem";
import { getFood } from "../../../apiCalls/apiCalls";

import { Link } from "react-router-dom";

export class Menu extends Component {
  constructor() {
    super();

    this.state = {
      foodItems: [],
      totalPrice: 0,
    };
  }

  incrementHandler = (name, price) => {
    if (this.state[name]) {
      this.setState(
        {
          [name]: (this.state[name] += 1),
          totalPrice: (this.state.totalPrice += price),
        },
        () => {
          console.log(this.state);
        }
      );
    } else {
      this.setState(
        { [name]: 1, totalPrice: (this.state.totalPrice += price) },
        () => {
          console.log(this.state);
        }
      );
    }
  };

  decrementHandler = (name, price) => {
    if (this.state[name] == 0 || this.state[name] == undefined) {
    } else if (this.state[name]) {
      this.setState(
        {
          [name]: (this.state[name] -= 1),
          totalPrice: (this.state.totalPrice -= price),
        },
        () => {
          console.log(this.state);
        }
      );
    }
  };

  async componentDidMount() {
    const foodData = await getFood(this.props.restaurantID);
    this.setState({ foodItems: foodData.food });
  }

  render() {
    const allFoods = this.state.foodItems.map((item) => {
      return (
        <MenuItem
          incrementHandler={this.incrementHandler}
          decrementHandler={this.decrementHandler}
          name={item.food_name}
          key={item.food_id}
          cuisine={item.cuisine}
          price={item.price}
          id={item.food_id}
          quantity={this.state[item.food_name]}
          user={this.props.user}
        />
      );
    });

    return (
      <div className="container-lg">
        <h3 className="p-0">Order Online</h3>
        <div className="d-flex sticky-top m-1 justify-content-center align-items-center rounded py-1 bg-danger">
          <div className="pr-3 text-white">
            Total Price : Rs. {this.state.totalPrice}
          </div>
          <button className=" bg-white rounded border-0 py-1 px-1">
            <div className="d-flex ">
              {this.props.user ? (
                <div>
                  <Link
                    to={{
                      pathname: "/checkout",
                      state: {
                        previousState: { ...this.state, foodItems: [] },
                      },
                      className: "text-decoration-none rounded text-white",
                    }}
                  >
                    Checkout
                  </Link>
                </div>
              ) : (
                <>Please Login</>
              )}

              <div>
                <i class="fas fa-shopping-cart"></i>
              </div>
            </div>
          </button>
        </div>

        {allFoods}
      </div>
    );
  }
}

export default Menu;
