import React, { Component } from "react";

export class MenuItem extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const { incrementHandler, decrementHandler, price, name } = this.props;

    return (
      <div className="container-lg mb-2">
        <div className="row">
          <div className="col-2">
            <img
              alt="2 Chicken Whopper + 1 King Fries + 1 Chicken fries + 2 Pepsi Can"
              src="https://b.zmtcdn.com/data/dish_photos/79a/18fd5dde339078ab4b6f846a08c2779a.jpg?fit=around|130:130&amp;crop=130:130;*,*"
              className="rounded"
            />
          </div>
          <div className="col-8">
            <div className="row">{name}</div>
            <div className="row">Rs. {price}</div>
          </div>
          <div className="col-2">
            <div className="btn-group" role="group" aria-label="Basic example">
              {/* <button
                type="button"
                className="btn btn-danger"
                onClick={() => {
                  decrementHandler(name, price);
                }}
              >
                -
              </button> */}

              {this.props.user ? (
                <>
                  <button
                    type="button"
                    className="btn btn-danger"
                    onClick={() => {
                      decrementHandler(name, price);
                    }}
                  >
                    -
                  </button>
                </>
              ) : (
                <></>
              )}

              <button type="button" className="btn btn-danger">
                {!this.props.quantity ? 0 : this.props.quantity}
              </button>

              {/* <button
                type="button"
                className="btn btn-danger"
                onClick={() => {
                  incrementHandler(name, price);
                }}
              >
                +
              </button> */}

              {this.props.user ? (
                <>
                  <button
                    type="button"
                    className="btn btn-danger"
                    onClick={() => {
                      incrementHandler(name, price);
                    }}
                  >
                    +
                  </button>
                </>
              ) : (
                <></>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default MenuItem;
