import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Home from "./Components/Home/Home";
import Footer from "./Components/Footer/Footer";
import Navbar from "./Components/Navigation/Navbar";

import RestuarantPage from "./Components/RestuarantPage/RestuarantPage";
import Checkout from "./Components/CheckoutPage/Checkout";
import API from "./BaseURL";

export class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: null,
    };
  }

  verifyUser = (accessToken) => {
    API.get("/user/authenticate", {
      headers: { Authorization: `Bearer ${accessToken}` },
    })
      .then((res) => {
        API.defaults.headers.common["Authorization"] = `Bearer ${accessToken}`;
        this.setState({
          user: res.data.userDetails,
        });
      })
      .catch(() => {
        localStorage.removeItem("accessToken");
      });
  };

  componentDidMount() {
    let accessToken = localStorage.getItem("token");

    if (accessToken) {
      this.verifyUser(accessToken);
    }
  }

  render() {
    return (
      <div>
        <BrowserRouter>
          <Switch>
            <Route
              exact
              path="/"
              render={(props) => (
                <>
                  <Navbar
                    user={this.state.user}
                    verifyUser={this.verifyUser}
                    {...props}
                  />
                  <Home
                    user={this.state.user}
                    verifyUser={this.verifyUser}
                    {...props}
                  />
                  <Footer />
                </>
              )}
            />

            <Route
              path="/restaurant/:restaurantID/order"
              render={(props) => (
                <>
                  <Navbar
                    user={this.state.user}
                    verifyUser={this.verifyUser}
                    {...props}
                  />
                  <RestuarantPage
                    user={this.state.user}
                    verifyUser={this.verifyUser}
                    {...props}
                  />
                  <Footer />
                </>
              )}
            />

            <Route
              path="/checkout"
              render={(props) => (
                <>
                  <Navbar
                    user={this.state.user}
                    verifyUser={this.verifyUser}
                    {...props}
                  />
                  <Checkout
                    user={this.state.user}
                    verifyUser={this.verifyUser}
                    {...props}
                  />
                  <Footer />
                </>
              )}
            />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
