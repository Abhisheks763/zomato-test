import API from "../BaseURL";
export const getCities = async () => {
  const initialData = await API.get(`/cities`);
  const cities = initialData.data;
  return cities;
};

export const getStates = async () => {
  const initialData = await API.get(`/states`);
  const states = initialData.data;
  return states;
};

export const getRestaurants = async () => {
  const initialData = await API.get(`/restaurants`);
  const restaurants = initialData.data;
  return restaurants;
};

// /restaurant/:restaurantid

export const getOneRestaurant = async (restaurantID) => {
  const initialData = await API.get(`/restaurant/${restaurantID}`);
  const restaurant = initialData.data;
  return restaurant;
};

export const getFood = async (restaurantID) => {
  const initialData = await API.get(`/restaurant/${restaurantID}/order`);
  const foods = initialData.data;
  return foods;
};
